const mongoose = require('mongoose');
const path = require("path");
const mime = require('mime-types');
const config = require("../config");

const Schema = mongoose.Schema;

const imageMimeTypes = ['image/jpeg', 'image/png', 'image/gif'];


const ImageSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    image: {
        type: String,
        validate: {
            validator: function(value) {
                const filePath = path.join(config.uploadPath, value);
                const mimeType = mime.lookup(filePath);
                return imageMimeTypes.includes(mimeType);
            },
            message: 'Image file format is incorrect'
        }
    },
    title: {
        type: String,
        required: true
    }
});

const Image = mongoose.model('Image', ImageSchema);

module.exports = Image;