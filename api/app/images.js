const express = require("express");
const Image = require("../models/Image");
const path = require("path");
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const auth = require('../middleware/auth');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    if (req.query.id) {
        const images = await Image.find({user: req.query.id}).populate('user', '_id userName');
        res.send(images);
    } else {
        const images = await Image.find().populate('user', '_id userName');
        res.send(images);
    }

});

router.post("/", auth, upload.single('image'), async (req, res, next) => {
    try {
        const imageData = {
            user: req.user._id,
            title: req.body.title,
            image: req.file ? req.file.filename : null,
        }
        const image = new Image(imageData);
        await image.save();
        return res.send(image);
    } catch (e) {
        next(e);
    }
});

router.delete('/:id', auth, async (req, res, next) => {
    try {
            const removedImage = await Image.deleteOne({_id: req.params.id});
            res.send(removedImage);
    } catch (e) {
        next(e);
    }
});

module.exports = router;
