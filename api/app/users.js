const express = require('express');
const mongoose = require("mongoose");
const config = require("../config");
const {nanoid} = require("nanoid");
const axios = require("axios");
const User = require("../models/User");


const router = express.Router();


router.get('/', async (req, res) => {
    const users = await User.find();
    res.send(users);
})

router.post('/', async (req, res, next) => {
    try {
        const userData = {
            email: req.body.email,
            password: req.body.password,
            userName: req.body.userName
        }

        const user = new User(userData);
        user.generateToken();
        await user.save();
        return res.send(user);
    } catch (e) {
        if (e instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(e);
        }
        return next(e);
    }
});

router.post('/sessions', async (req, res, next) => {
    try {
        const user = await User.findOne({email: req.body.email});
        if (!user) {
            return res.status(400).send({message: 'User is not found'})
        }
        const isMatch = await user.checkPassword(req.body.password)
        if (!isMatch) {
            return res.status(400).send('Password is incorrect');
        }
        user.generateToken();
        await user.save();
        return res.send(user);

    } catch (e) {
        next(e);
    }
});

router.delete('/sessions', async (req, res, next) => {
    try {
        const token = req.get('Authorization');
        const message = {message: 'OK'};

        if (!token) return res.send(message);

        const user = await User.findOne({token});

        if (!user) return res.send(message);

        user.generateToken();
        await user.save();

        return res.send(message);
    } catch (e) {
        next(e);
    }
});

router.post('/fbLogin', async (req, res) => {
    try {
        const inputToken = req.body.authToken;
        const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
        const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
        const response = await axios.get(debugTokenUrl);

        if (response.data.data.error) {
            return res.status(401).send({message: 'Facebook token incorrect'});
        }

        if (req.body.id !== response.data.data.user_id) {
            return res.status(401).send({message: 'Wrong user ID'});
        }

        let user = await User.findOne({facebookID: req.body.id});
        if (!user) {
            user = new User({
                email: req.body.email,
                password: nanoid(),
                facebookID: req.body.id,
                userName: req.body.userName,
            });
        }

        user.generateToken();
        await user.save();
        return res.send(user);
    } catch (error) {
        return res.status(401).send({message: 'Facebook token incorrect'});
    }
});


module.exports = router;