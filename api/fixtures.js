const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Image = require("./models/Image");
const {nanoid} = require("nanoid");

const run = async () => {

    await mongoose.connect(config.mongo.db, config.mongo.options);
    const collections = await mongoose.connection.db.listCollections().toArray()

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [Roman, Ivan] = await User.create(
        {
            email: 'roman@mail.com',
            password: '123',
            userName: 'roma',
            role: 'admin',
            token: nanoid(),
            facebookID: '123'
        },
        {
            email: 'ivan@mail.com',
            password: '321',
            userName: 'Vanya',
            role: 'user',
            token: nanoid(),
            facebookID: '321'
        }
    )
    await Image.create(
        {
            user: Roman,
            image: '3.jpg',
            title: 'Sunset',
        },
        {
            user: Ivan,
            image: '1.jpg',
            title: 'Paris',
        },
        {
            user: Roman,
            image: '2.jpg',
            title: 'A drop',
        },
        {
            user: Roman,
            image: '4.jpg',
            title: 'Tokyo',
        },
    )

    await mongoose.connection.close();
}
run().catch(e => console.error(e))