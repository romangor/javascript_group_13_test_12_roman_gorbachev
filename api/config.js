const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    mongo: {
        db: 'mongodb://localhost/gallery',
        options: {useNewUrlParser: true},
    },
    facebook: {
        appId: '313807987527267',
        appSecret: '15bf405bb7a2399e6e977297d385f2dd'
    }
};