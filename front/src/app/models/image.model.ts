export class Image {
  constructor(
    public _id: string,
    public user: {
      _id: string,
      userName: string,
    },
    public title: string,
    public image: string,
  ) {
  }
}

export interface imageError {
  error: string
}

export interface imageData {
  title: string,
  image: File | null
}
