export interface User {
  _id: string,
  email: string,
  token: string,
  userName: string,
}

export interface registerData {
  email: string,
  password: string,
  userName: string,

}

export interface FieldError {
  message: string
}

export interface registerError {
  errors: {
    password: FieldError,
    email: FieldError
  }
}

export interface loginData {
  email: string,
  password: string,
}

export interface loginFB {
  authToken: string,
  id: string,
  email: string,
  userName: string,
}

export interface loginError {
  error: string
}
