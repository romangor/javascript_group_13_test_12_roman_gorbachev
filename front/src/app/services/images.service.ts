import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Image, imageData } from '../models/image.model';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {
  constructor(private http: HttpClient) {
  }

  fetchImages(id: string) {
    return this.http.get<Image[]>(environment.url + `/images?id=${id}`)
      .pipe(
        map(response => {
          return response.map(imageData => {
            return new Image(
              imageData._id,
              imageData.user,
              imageData.title,
              imageData.image
            );
          });
        })
      )
  };

  createImage(image: imageData) {
    const formData = new FormData();
    formData.append('title', image.title);
    if (image.image) {
      formData.append('image', image.image);
    }
    return this.http.post(environment.url + '/images', formData);
  };


  removeImage(imageId: string) {
    return this.http.delete(`${environment.url}/images/${imageId}`);
  };

}
