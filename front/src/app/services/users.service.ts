import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { loginData, loginFB, registerData, User } from '../models/user.model';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UsersServices {
  constructor(private http: HttpClient) {
  }

  registrationUser(user: registerData) {
    const userData = {
      email: user.email,
      password: user.password,
      userName: user.userName,
    }
    return this.http.post<User>(environment.url + '/users', userData);
  };

  login(userData: loginData) {
    return this.http.post<User>(environment.url + '/users/sessions', userData);
  };

  loginWithFacebook(userFB: loginFB) {
    return this.http.post<User>(environment.url + '/users/fbLogin', userFB);
  }

  logout() {
    return this.http.delete(environment.url + '/users/sessions')
  };

}

