import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HelpersService } from '../services/helper.service';
import { Router } from '@angular/router';
import {
  createImageFailure,
  createImageRequest,
  createImageSuccess,
  fetchImagesFailure,
  fetchImagesRequest,
  fetchImagesSuccess, removeImageFailure, removeImageRequest, removeImageSuccess
} from './images.actions';
import { catchError, mergeMap, Observable, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { ImagesService } from '../services/images.service';
import { AppState } from './types';
import { Store } from '@ngrx/store';
import { User } from '../models/user.model';

@Injectable()

export class ImagesEffects {
  user: Observable<null | User>;

  fetchImages = createEffect(() => this.actions.pipe(
    ofType(fetchImagesRequest),
    mergeMap(({id}) => this.imagesService.fetchImages(id).pipe(
        map((images) => fetchImagesSuccess({images})),
        catchError(() => of(fetchImagesFailure({error: 'error'})))
      )
    )));


  createImage = createEffect(() => this.actions.pipe(
    ofType(createImageRequest),
    mergeMap(({image}) => this.imagesService.createImage(image).pipe(
      map(() => createImageSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Image added successfully');
        void this.router.navigate(['/']);
      }),
      catchError(() => of(createImageFailure({error: 'Error creating image'})))
    ))
  ));

  removeImage = createEffect(() => this.actions.pipe(
    ofType(removeImageRequest),
    mergeMap(({imageId}) => this.imagesService.removeImage(imageId).pipe(
      map(() => removeImageSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Image removed');
        this.store.dispatch(fetchImagesRequest({id: ''}))
      }),
      catchError(() => of(removeImageFailure({error: 'You cannot delete this image'}))
      )))
  ));


  constructor(
    private actions: Actions,
    private helpers: HelpersService,
    private imagesService: ImagesService,
    private router: Router,
    private store: Store<AppState>
  ) {
    this.user = store.select(state => state.users.user);
  }
}
