import { loginError, registerError, User } from '../models/user.model';
import { Image, imageError } from '../models/image.model';

export type UsersState = {
  user: User | null,
  registerLoading: boolean,
  registerError: registerError | null,
  loginLoading: boolean,
  loginError: null | loginError,
}

export type ImagesState = {
  images: Image[],
  imagesLoading: boolean,
  imagesError: null | imageError
}

export type AppState = {
  users: UsersState,
  images: ImagesState
}

