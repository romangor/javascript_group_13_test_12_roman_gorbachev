import { createAction, props } from '@ngrx/store';
import { loginData, loginError, loginFB, registerData, registerError, User } from '../models/user.model';

export const registerUserRequest = createAction(
  '[Users] Register Request',
  props<{ user: registerData }>()
);
export const registerUserSuccess = createAction(
  '[Users] Register Success',
  props<{ user: User }>()
);
export const registerUserFailure = createAction(
  '[Users] Register Failure',
  props<{ error: null | registerError }>()
);

export const loginUserRequest = createAction(
  '[Users] Login Request',
  props<{ userData: loginData }>()
);
export const loginUserSuccess = createAction(
  '[Users] Login Success',
  props<{ user: User }>()
);
export const loginUserFailure = createAction(
  '[Users] Login Failure',
  props<{ error: null | loginError }>()
);

export const logoutUser = createAction('[Users] Logout');
export const logoutUserRequest = createAction('[Users] Server Logout Request');

export const loginWithFacebookRequest = createAction('[User] LoginFb Request', props<{ userFb: loginFB }>());
export const loginWithFacebookSuccess = createAction('[User] LoginFB Successful', props<{ user: User }>());
export const loginWithFacebookFailure = createAction('[User] LoginFB Failure', props<{ error: null | loginError }>());

