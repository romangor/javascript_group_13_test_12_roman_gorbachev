import { createAction, props } from '@ngrx/store';
import { Image, imageData } from '../models/image.model';

export const fetchImagesRequest = createAction('[Images] Fetch Request', props<{ id: string }>());
export const fetchImagesSuccess = createAction('[Images] Fetch Success', props<{ images: Image[] }>());
export const fetchImagesFailure = createAction('[Images] Fetch Failure', props<{ error: string }>());

export const createImageRequest = createAction('[Image] Create Request', props<{ image: imageData }>());
export const createImageSuccess = createAction('[Image] Create Success');
export const createImageFailure = createAction('[Image] Create Failure', props<{ error: string }>());

export const removeImageRequest = createAction('[Image] Remove Request', props<{ imageId: string }>());
export const removeImageSuccess = createAction('[Image] Remove Success');
export const removeImageFailure = createAction('[Image] Remove Failure', props<{ error: string }>());
