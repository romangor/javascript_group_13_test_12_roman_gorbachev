import { Injectable } from '@angular/core';
import { UsersServices } from '../services/users.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess,
  loginWithFacebookFailure,
  loginWithFacebookRequest,
  loginWithFacebookSuccess,
  logoutUser,
  logoutUserRequest,
  registerUserFailure,
  registerUserRequest,
  registerUserSuccess
} from './users.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { HelpersService } from '../services/helper.service';

@Injectable()
export class UsersEffects {
  constructor(
    private actions: Actions,
    private usersService: UsersServices,
    private router: Router,
    private snackbar: MatSnackBar,
    private helpers: HelpersService
  ) {
  }

  registrationUser = createEffect(() => this.actions.pipe(
    ofType(registerUserRequest),
    mergeMap(({user}) => this.usersService.registrationUser(user).pipe(
      map((user) => registerUserSuccess({user})),
      tap(() => {
        void this.router.navigate((['/']));
        this.snackbar.open('Register successful', 'Ok', {duration: 3000});
      }),
      catchError(reqErr => {
        let registerError = null;
        if (reqErr instanceof HttpErrorResponse && reqErr.status === 400) {
          registerError = reqErr.error;
          this.snackbar.open('This user already exists', 'OK', {duration: 3000})
        } else {
          this.snackbar.open('Server error', 'OK', {duration: 3000})
        }
        return of(registerUserFailure({error: registerError}));
      })
    ))
  ))

  loginUser = createEffect(() => this.actions.pipe(
    ofType(loginUserRequest),
    mergeMap(({userData}) => this.usersService.login(userData).pipe(
      map(user => loginUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackbar('Login successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(loginUserFailure)
    ))
  ));

  loginUserFB = createEffect(() => this.actions.pipe(
    ofType(loginWithFacebookRequest),
    mergeMap(({userFb}) => this.usersService.loginWithFacebook(userFb).pipe(
      map(user => loginWithFacebookSuccess({user})),
      tap(() => {
        this.helpers.openSnackbar('Login successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(loginWithFacebookFailure)
    ))
  ));

  logoutUser = createEffect(() => this.actions.pipe(
    ofType(logoutUserRequest),
    mergeMap(() => {
      return this.usersService.logout().pipe(
        map(() => logoutUser()),
        tap(() => {
          this.helpers.openSnackbar('Logout successful')
          void this.router.navigate(['/']);
        })
      );
    }))
  )
}


