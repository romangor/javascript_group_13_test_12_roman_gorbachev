import { ImagesState } from './types';
import { createReducer, on } from '@ngrx/store';
import { fetchImagesFailure, fetchImagesRequest, fetchImagesSuccess } from './images.actions';


const initialState: ImagesState = {
  images: [],
  imagesLoading: false,
  imagesError: null
}

export const imagesReducer = createReducer(
  initialState,
  on(fetchImagesRequest, state => ({...state, fetchLoadings: true})),
  on(fetchImagesSuccess, (state, {images}) => ({...state, fetchLoadings: false, images})),
  on(fetchImagesFailure, (state, {error}) => ({...state, fetchLoadings: false, fetchError: error})),
);
