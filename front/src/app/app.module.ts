import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MainComponent } from './pages/main/main.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './pages/login/login.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { CenteredCardComponent } from './ui/centered-card/centered-card.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FlexModule } from '@angular/flex-layout';
import { UsersEffects } from './store/users.effects';
import { usersReducer } from './store/users.reducer';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './auth.interceptor';
import { environment } from '../environments/environment';
import { localStorageSync } from 'ngrx-store-localstorage';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { ValidateIdenticalDirective } from './directives/paswoord-validete.directive';
import { ImagesEffects } from './store/images.effects';
import { imagesReducer } from './store/images.reducer';
import { CreateImageComponent } from './pages/create-image/create-image.component';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { MyGalleryComponent } from './pages/my-gallery/my-gallery.component';
import { UserTypeDirective } from './directives/user-type.derective';
import { CheckUser } from './directives/chek-user-directive';
import { ModalComponent } from './ui/modal/modal.component';
import { UserGalleryComponent } from './pages/user-gallery/user-gallery.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.fbClientId, {
        scope: 'email, public_profile',
      })
    }
  ]
};

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}
const metaReducers: MetaReducer[] = [localStorageSyncReducer];

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    MainComponent,
    LoginComponent,
    RegistrationComponent,
    CenteredCardComponent,
    ValidateIdenticalDirective,
    CreateImageComponent,
    FileInputComponent,
    MyGalleryComponent,
    UserTypeDirective,
    CheckUser,
    ModalComponent,
    UserGalleryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    FormsModule,
    MatSnackBarModule,
    StoreModule.forRoot({users: usersReducer, images: imagesReducer}, {metaReducers}),
    EffectsModule.forRoot([UsersEffects, ImagesEffects]),
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    FlexModule,
    HttpClientModule,
    SocialLoginModule,
    MatDialogModule,
    MatProgressSpinnerModule

  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: 'SocialAuthServiceConfig', useValue: socialConfig}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
