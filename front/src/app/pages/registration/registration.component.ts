import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { registerError } from '../../models/user.model';
import { registerUserRequest } from '../../store/users.actions';
import { AppState } from '../../store/types';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements AfterViewInit, OnDestroy {
  @ViewChild('f') form!: NgForm;

  loading: Observable<boolean>;
  error: Observable<null | registerError>;
  errorSub!: Subscription;

  constructor(private store: Store<AppState>) {
    this.error = store.select(state => state.users.registerError);
    this.loading = store.select(state => state.users.registerLoading);
  }

  ngAfterViewInit(): void {
    this.errorSub = this.error.subscribe(error => {
      if (error) {
        const message = error.errors.email.message;
        this.form.form.get('email')?.setErrors({serverError: message});
      } else {
        this.form.form.get('email')?.setErrors({});
      }
    });
  }

  register() {
    const userData = {
      email: this.form.value.email,
      password: this.form.value.password,
      userName: this.form.value.userName,
    };
    this.store.dispatch(registerUserRequest({user: userData}));
  }

  ngOnDestroy() {
    this.errorSub.unsubscribe();
  }


}
