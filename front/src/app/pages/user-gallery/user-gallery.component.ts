import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Image, imageError } from '../../models/image.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchImagesRequest } from '../../store/images.actions';
import { User } from '../../models/user.model';
import { ActivatedRoute } from '@angular/router';
import { ModalComponent } from '../../ui/modal/modal.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-user-gallery',
  templateUrl: './user-gallery.component.html',
  styleUrls: ['./user-gallery.component.sass']
})
export class UserGalleryComponent implements OnInit {
  images: Observable<Image[]>;
  loading: Observable<boolean>;
  error: Observable<null | imageError>;
  subscribe!: Subscription;
  user: Observable<null | User>;
  userData!: User;
  userId!: string;
  userName!: string

  constructor(private store: Store<AppState>, private route: ActivatedRoute, public dialog: MatDialog) {
    this.images = store.select(state => state.images.images);
    this.loading = store.select(state => state.images.imagesLoading);
    this.error = store.select(state => state.images.imagesError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.images.subscribe(images => {
      images.forEach(image => {
        this.userName = image.user.userName
      })
    })
    this.user.subscribe(user => {
      this.userData = user!
    })
    this.subscribe = this.route.queryParams.subscribe(params => {
      this.userId = params['id'];
    });
    this.store.dispatch(fetchImagesRequest({id: this.userId}));
  }

  openModal(imagePath: string) {
    this.dialog.open(ModalComponent, {
      data: {image: imagePath},
    });
  }

}
