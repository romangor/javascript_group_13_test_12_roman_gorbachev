import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { createImageRequest } from '../../store/images.actions';
import { imageData } from '../../models/image.model';


@Component({
  selector: 'app-create-image',
  templateUrl: './create-image.component.html',
  styleUrls: ['./create-image.component.sass']
})


export class CreateImageComponent {
  @ViewChild('f') form!: NgForm;

  constructor(private store: Store<AppState>) {
  }

  create() {
    const image: imageData = this.form.value;
    this.store.dispatch(createImageRequest({image}));
  }
}
