import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Image, imageError } from '../../models/image.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchImagesRequest, removeImageRequest } from '../../store/images.actions';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../../ui/modal/modal.component';

@Component({
  selector: 'app-my-gallery',
  templateUrl: './my-gallery.component.html',
  styleUrls: ['./my-gallery.component.sass']
})
export class MyGalleryComponent implements OnInit {
  images: Observable<Image[]>;
  loading: Observable<boolean>;
  error: Observable<null | imageError>;

  constructor(private store: Store<AppState>, public dialog: MatDialog) {
    this.images = store.select(state => state.images.images);
    this.loading = store.select(state => state.images.imagesLoading);
    this.error = store.select(state => state.images.imagesError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchImagesRequest({id: ''}));
  }

  remove(imageId: string) {
    this.store.dispatch(removeImageRequest({imageId: imageId}));
  }

  openModal(imagePath: string) {
    const dialogRef = this.dialog.open(ModalComponent, {
      data: {image: imagePath},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }


}
