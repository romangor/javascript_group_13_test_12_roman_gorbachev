import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { loginData, loginError, loginFB } from '../../models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { FacebookLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { loginUserRequest, loginWithFacebookRequest } from '../../store/users.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | loginError>;
  authStateSub!: Subscription;


  constructor(private store: Store<AppState>, private authService: SocialAuthService) {
    this.loading = store.select(state => state.users.loginLoading);
    this.error = store.select(state => state.users.loginError);
  }

  ngOnInit() {
    this.authStateSub = this.authService.authState.subscribe((user: SocialUser) => {
      const userFb: loginFB = {
        authToken: user.authToken,
        id: user.id,
        email: user.email,
        userName: user.name,
      };
      this.store.dispatch(loginWithFacebookRequest({userFb}))
    });

  }

  login() {
    const userData: loginData = this.form.value;
    this.store.dispatch(loginUserRequest({userData}));
  };

  fbLogin() {
    void this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  };


  ngOnDestroy() {
    this.authStateSub.unsubscribe();
  }

}
