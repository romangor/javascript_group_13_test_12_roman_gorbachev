import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Image, imageError } from '../../models/image.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchImagesRequest } from '../../store/images.actions';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../../ui/modal/modal.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})

export class MainComponent implements OnInit {
  images: Observable<Image[]>;
  loading: Observable<boolean>;
  error: Observable<null | imageError>;

  constructor(private store: Store<AppState>, public dialog: MatDialog) {
    this.images = store.select(state => state.images.images);
    this.loading = store.select(state => state.images.imagesLoading);
    this.error = store.select(state => state.images.imagesError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchImagesRequest({id: ''}));
  }

  openModal(imagePath: string) {
    this.dialog.open(ModalComponent, {
      data: {image: imagePath},
    });
  }

}
