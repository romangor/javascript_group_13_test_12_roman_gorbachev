import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateImageComponent } from './pages/create-image/create-image.component';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/main/main.component';
import { MyGalleryComponent } from './pages/my-gallery/my-gallery.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { UserGalleryComponent } from './pages/user-gallery/user-gallery.component';

const routes: Routes = [
  {path: '', component: MainComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegistrationComponent},
  {path: 'create', component: CreateImageComponent},
  {path: 'my-gallery', component: MyGalleryComponent},
  {path: 'user-gallery', component: UserGalleryComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
