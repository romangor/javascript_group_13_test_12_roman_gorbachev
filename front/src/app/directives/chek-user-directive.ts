import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { Observable, Subscription } from 'rxjs';
import { User } from '../models/user.model';

@Directive({
  selector: '[appCheckUser]'
})
export class CheckUser implements OnInit, OnDestroy {
  user: Observable<User | null>;
  userSubscription!: Subscription;
  @Input('appCheckUser') userID?: string

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private store: Store<AppState>
  ) {
    this.user = store.select(state => state.users.user);
  }

  ngOnInit() {
    this.userSubscription = this.user.subscribe(user => {
      this.viewContainer.clear();
      if (user?._id === this.userID) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      }
    })
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }
}
